package com.zcy.jkdemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: zcy
 * @Date: 2020/08/02/21:00
 * @Description:
 */
@RestController
public class DemoController {
    @GetMapping("/justDemo")
    public String justDemo(){
        return  "hello world";
    }
}
