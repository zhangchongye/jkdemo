package com.zcy.jkdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JkdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(JkdemoApplication.class, args);
    }

}
